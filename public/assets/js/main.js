"use strict";
var test_mode = false;
var selected_index = 0;
document.addEventListener("DOMContentLoaded", function() {
    var responseTitleElem = document.getElementById('response-title');
    var responseSubTitleElem = document.getElementById('response-subtitle');
    var responseMonth = document.getElementById('response-month');

    // init 
    setResponse();

    function DisplayResponse(response){
        responseTitleElem.innerText = response.title;
        responseSubTitleElem.innerText = response.subtitle;
        responseMonth.innerText = response.monthString;

    }

    function setResponse(index){
        if(index != undefined)
            DisplayResponse(responses[index]);
        else{
            for(var i = 0; i < responses.length; i++){
                if(responses[i].month.getMonth() == new Date().getMonth()){
                    selected_index = i;
                    DisplayResponse(responses[i]);
                    break;
                }
            }
        }
    }
        
    document.addEventListener('keyup',
        Konami.code(function() {
            test_mode = !test_mode;
            if(test_mode)
                document.addEventListener('keydown', LoopMonth);
            else{
                document.removeEventListener('keydown', LoopMonth);
                setResponse();
            }
        })
    );
        function LoopMonth(e){
            var max = responses.length - 1
            switch (e.keyCode) {
                case 38:
                    // Up
                    selected_index = selected_index == max ? 0 : selected_index + 1;
                    break;
                case 40:
                    // Down
                    selected_index = selected_index == 0 ? max : selected_index - 1;
                    break;
            }
            setResponse(selected_index);
        }
  });


//Reponse
class Response{

    constructor(month, title, subtitle){
        this.monthString = month;
       switch (month) {
            case 'Janvier':
                month = 0;
                break;
            case 'Février':
                month = 1;
                break; 
            case 'Mars':
                month = 2;
                break; 
            case 'Avril':
                month = 3;
                break;
            case 'Mai':
                month = 4;
                break;
            case 'Juin':
                month = 5;
                break;
            case 'Juillet':
                month = 6;
                break; 
            case 'Août':
                month = 7;
                break; 
            case 'Septembre':
                month = 8;
                break; 
            case 'Octobre':
                month = 9;
                break; 
            case 'Novembre':
                month = 10;
                break; 
            case 'Décembre':
                month = 11;
                break; 
            default:
                break;
        }
        this.month = new Date(null, month);
        this.title = title;
        this.subtitle = subtitle;
    }

}

// Responses
var responses = [
    new Response("Janvier","Non","Mais ça approche !"),
    new Response("Février","Non","Mais c'est pour très bientôt !"),
    new Response("Mars","Oui","Prends un pot plus grand, un plantoir, du terreau et fonce champion·ne !"),
    new Response("Avril","Oui","Perds pas de temps, après il sera trop tard..."),
    new Response("Mai","Trop Tard!","T'es à la bourre mais tu peux toujours te rattraper en septembre."),
    new Response("Juin","Non","Mais c'est bientôt les vacances."),
    new Response("Juillet","Non","Par contre, c'est peut-être l'heure de l'apéro."),
    new Response("Août","Non","Mais n'oublie pas de bien arroser."),
    new Response("Septembre","Oui","C'est ta dernière chance avant l'année prochaine. Je dis ça, je dis rien."),
    new Response("Octobre","Non","T'as loupé le coche. RDV en mars. ¯|_ (ツ)_/¯"),
    new Response("Novembre","Non","Wesh tu manges de la raclette en août toi ?"),
    new Response("Décembre","Non","Tes plantes sont en hivernage, concentre-toi !")
]